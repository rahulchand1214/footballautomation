package com.bwin.Automation.Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

public class CommonUtilities
{
    public static Properties properties = new Properties();
    public File f;
    public FileInputStream fI;

    /**
     * This method use for generate random value from common utilities
     * @return
     */
    public static String getRendomValue(){
        Random random = new Random();
        int randomInt = random.nextInt(9999);
        return Integer.toString(randomInt);
    }

    /**
     * This method will use to load config.property file from common utilities
     */
    public void loadPropertiesFile(){
        f = new File("config.properties");
        try {
            fI = new FileInputStream(f);
            properties.load(fI);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This method will use for date stamp
     * @return
     */
    public static String dateStamp(){
        DateFormat dateFormat = new SimpleDateFormat("ddMMhhss");
        Date date = new Date();
        String date1 = dateFormat.format(date);
        return date1;
    }

    /**
     * This method will use for date format
     * @return
     */
    public static String dateFormat(){
        String t ="T";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String date1 = dateFormat.format(date);
        return date1;
    }

    /**
     * This method will use for date format
     * @return
     */
    public static String dateFormatWithTwoDaysLater(){
        String t ="T";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String date1 = dateFormat.format(date);
        return date1;
    }

    /**
     * This method will use for time format
     * @return
     */
    public static String timeFormat(){
        String t ="T";
        DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
        Date date = new Date();
        String date1 = dateFormat.format(date);
        return date1;
    }

    /**
     * This method will give current date format for event start date
     * @return
     */
    public static String eventStartDateFormat(){
        String format = dateFormat()+"'T'"+timeFormat()+"'Z'";
        return format;
    }

    /**
     * This method will give current date format for event start date
     * @return
     */
    public static String eventEndDateFormat(){
        String format = dateFormat()+"'T'"+timeFormat()+"'Z'";
        return format;
    }

    public static String getHostFromURI(String baseUri){
        String host = "";
        try {
            URL aUrl = new URL(baseUri);
            host = aUrl.getHost();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return host;
    }
    
    public Properties loadProperties(){
    	Properties props=new Properties();
        f = new File("config.properties");
        try {
            fI = new FileInputStream(f);
            props.load(fI);
        }catch (Exception e){
            e.printStackTrace();
        }
		return props;
    }

}
