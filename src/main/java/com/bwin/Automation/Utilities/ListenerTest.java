package com.bwin.Automation.Utilities;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ListenerTest implements ITestListener{

	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("\033[34m"+"Test Started->"+"\033[34m" +"\033[34m"+result.getName()+"\033[34m");
	}

	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		 System.out.println("\033[32;1;2m"+"Test Pass->"+"\033[0m" +"\033[32;1;2m"+result.getName()+"\033[0m");
	}

	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		System.out.println("\033[31;1m"+"Test Failed->"+"\033[0m" +"\033[31;1m"+result.getName()+"\033[0m");
	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		System.out.println("END Of Execution(TEST)->"+context.getName());
	}

}
