package com.bwin.Automation.Utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


/**
 * @author srinivas Rajole
 +------------------------------------------------------------+
 | Module Name: TestBase                                     |
 | Module Purpose: Base class for all the generic definitions|
+------------------------------------------------------------+
 */
public class TestBase {

	@BeforeSuite
	public static void initialisation() throws InterruptedException {
		System.out.println("****************************** This is Before Suite*******************");
	}

	@AfterSuite
	public static void tearDown() throws InterruptedException {
		System.out.println("****************************** This is After Suite*******************");
	}

	/**Method to to read the Config file data
	 * @param CONFIG_PATH
	 * @param key
	 * @return KeyValue
	 */
	public static String getProperty(String CONFIG_PATH, String key) {
		String KeyValue = "";
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(CONFIG_PATH));
			KeyValue = prop.getProperty(key);
		} catch (Exception e) {
			System.out.println("Exception in Reading Config File::" + e.getClass().getName());
		}

		return KeyValue;

	}

	public static String  getPathNames(String PropFilename)
	{
		String Path=".//src//test//resources";
		//switch (PropFilename) {
		//case "DataConfig":
			Path+= "//DataConfig.properties";
			return Path;

		//default:
			//System.out.println("Invalid Config path name Please provide a valid Config filename ");
			//break;
	//	}
	//Path+= "//DataConfig.properties";
		//return Path;
		
		
		
		
		
	}
	
	public static Properties properties = new Properties();

	/**Method to read Config data
	 * @throws Exception
	 */
	public void propertiesLoading() throws Exception {
		InputStream inputStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("DataConfig.properties");
		properties.load(inputStream);

	}

}
