package com.bwin.Automation.Utilities;

import java.util.Map;

import org.apache.kafka.common.serialization.Serializer;
import org.hamcrest.core.IsInstanceOf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;

import Bwin.Sports.ContentDistributionShipper.Contracts.FixtureOuterClass.Fixture;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.FixtureDeltaOuterClass.FixtureDelta;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.MarketContextDeltaOuterClass.MarketContextDelta;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.MarketDeltaOuterClass.MarketDelta;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.OptionContextDeltaOuterClass.OptionContextDelta;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.OptionDeltaOuterClass.OptionDelta;
public class ProtoSerializer implements Serializer<Object> {


    public byte[] serialize(String arg0, Object arg1) {
    	
    	
    	if(arg1 instanceof Fixture)
           return( (Fixture)(arg1)).toByteArray();
   
     
    if(arg1 instanceof FixtureDelta)
        return( (FixtureDelta)(arg1)).toByteArray();
    
    else if(arg1 instanceof MarketDelta)
    	return( (MarketDelta)(arg1)).toByteArray();
 	
    else if(arg1 instanceof MarketDelta)
    	return( (MarketDelta)(arg1)).toByteArray();

    else if(arg1 instanceof MarketContextDelta)
        return( (MarketContextDelta)(arg1)).toByteArray();
	
    else if(arg1 instanceof OptionDelta)
        return( (OptionDelta)(arg1)).toByteArray();
    
    else if(arg1 instanceof OptionContextDelta)
        return( (OptionContextDelta)(arg1)).toByteArray();
    
    return null;
 	
    }
         public void close() {
         }

		public void configure(Map<String, ?> configs, boolean isKey) {
			// TODO Auto-generated method stub
			
		}
        
    
}






