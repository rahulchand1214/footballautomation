package football;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.support.KafkaHeaders;

import com.google.protobuf.CodedOutputStream;
import com.bwin.Automation.Utilities.CommonUtilities;
import com.bwin.Automation.Utilities.ProtoSerializer;

import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.FixtureDeltaOuterClass.FixtureDelta;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.MarketContextDeltaOuterClass.MarketContextDelta;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.MarketDeltaOuterClass.MarketDelta;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.OptionContextDeltaOuterClass.OptionContextDelta;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.OptionDeltaOuterClass.OptionDelta;
import Bwin.Sports.ContentDistributionShipper.Contracts.FixtureOuterClass.Fixture;

public class KafkaProducerExample {
	static String topic;
	String bootstrap_servers, CLIENT_ID_CONFIG;
	private static Producer<String, Object> producer;


	private static Producer<String, Object> createProducer(Object msg) {
		Properties props = new Properties();
		props = new CommonUtilities().loadProperties();
		String host = props.getProperty("kafka_producer_bootstrap_servers");
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, host);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ProtoSerializer.class.getName());
		props.put("BWIN_OTHER_INPLAY_DATA_TOPIC", "kafka.inplayevent.topic");
		
/*
		  props.put("security.protocol","SSL"); 
		  props.put("ssl.truststore.type","JKS");
		  props.put("ssl.keystore.type", "JKS"); 
		  props.put("ssl.keystore.location", "C:\\IVY-Software\\ivy.keystore-nonprod.jks"); 
		  props.put("ssl.keystore.password", "changeit");
		  props.put("ssl.key.password","changeit");
		  props.put("ssl.truststore.location", "C:\\IVY-Software\\ivy.truststore-nonprod.jks");  
		  props.put("ssl.truststore.password", "changeit");
		
*/
		topic = props.getProperty("kafka.inplayevent.topic");
		return new KafkaProducer<String, Object>(props);
	}
	
	
	public static void sendMessage(long time, String key, Object msg,String type, String fixtureID)

			throws InterruptedException, ExecutionException, IllegalStateException, IOException {
		if (null == producer) {
			producer = createProducer(msg);
		}
		final ProducerRecord<String, Object> record = new ProducerRecord<String, Object>(topic, key, msg);
		Headers headers = record.headers();
		
	   	headers.add("Class", type.getBytes());
		headers.add("SequenceNumber", getByteForLong(3l));
		headers.add("Type", "Snapshot".getBytes());
		headers.add("FixtureId",fixtureID.getBytes());
		headers.add("SportId", getByteForInt(4));
		headers.add("CompetitionId", getByteForInt(2102));
		headers.add(KafkaHeaders.TOPIC, topic.getBytes());
		headers.add(KafkaHeaders.MESSAGE_KEY, key.getBytes());
		System.out.println("\n\t*******************************************************************\n\t");
		RecordMetadata metadata = producer.send(record).get();
		System.out.println("\n\t*******************************************************************\n\t");
		System.out.println("--------" + record);
		System.out.println("\n\t*******************************************************************\n\t");
		long elapsedTime = System.currentTimeMillis() - time;
		System.out.printf(
				"sent record(key=%s value=%s) " + "\n\t***********************************\n\t"
						+ "meta(partition=%d, offset=%d) time=%d",
				record.key(), record.value(), metadata.partition(), metadata.offset(), elapsedTime);
	}
	

	private static byte[] getByteForLong(Long x) throws IOException {
		ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        CodedOutputStream co = CodedOutputStream.newInstance(buffer);
        co.writeFixed64NoTag(x);
        return buffer.array();
    }
   
    private static byte[] getByteForInt(Integer x) throws IOException {
    	ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        CodedOutputStream co = CodedOutputStream.newInstance(buffer);
        co.writeFixed32NoTag(x);
        return buffer.array();
    }
	
	
	
}