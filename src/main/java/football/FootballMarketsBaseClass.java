package football;

import static org.hamcrest.CoreMatchers.equalTo;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import com.amazonaws.Response;
import com.google.gson.Gson;

import Bwin.Sports.ContentDistributionShipper.Contracts.FixtureOuterClass.Fixture;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.MarketContextDeltaOuterClass.MarketContextDelta;
import Bwin.Sports.ContentDistributionShipper.Contracts.Deltas.MarketDeltaOuterClass.MarketDelta;
import io.restassured.RestAssured;
import net.thucydides.core.annotations.Title;

public class FootballMarketsBaseClass {
	
	public static Properties properties = new Properties();
    public static String eventurl;
	public static Long bwinid = 16110136 + Long.parseLong(RandomStringUtils.randomNumeric(3));
	public static Long firstValue = bwinid;
	
	 public static String randomNumbers = RandomStringUtils.randomNumeric(3);
		public static String numbers = randomNumbers;

		Date date = new Date();
		public String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
	
	public static String Eventid = bwinid + "?includeUndisplayed=true";
	String eventInsertDelay, eventUpdateDelay;
	
	public static long inEventSleepTime, upEventSleepTime;
	public static String mCount, activeStatus, suspendStatus, displayStatus, selecCount, BwinDataToInsert,
			BwinDataAfterVoidResulting, ResultCodeID, value, BwinDataAfterStatusResulting, allResponse,
			RESULTED_RESPONSE ,Dartsdata5;
	public static int OptionCount;

	public static String pat = "\n===================================================================================\n";
	io.restassured.response.Response globalResponseActive;
	public static Response globalResponseSuspend, globalResponseSSResponse,
			globalResponseResultResponse;

	
	private static String readMockValues(Properties properties, String key) {
		return String.valueOf(properties.get(key));
	}
	public void LoadProperties() throws Exception {
	   InputStream inputStream = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("testFootballMarketTemplate.properties");
		properties.load(inputStream);
		eventurl = readMockValues(properties, "eventdetailsUrl");
		ResultCodeID = readMockValues(properties, "resultUrl");
	}

//==================================================================================================================================================================================================================================================================================
	
	@Title("TC_E001 || FootBall ||  - Verify Event Insert")
	@Test
	public void TC_E001_VerifyEventInsertAndMarketResult() throws Exception {
		
		LoadProperties();
		String FixtureData;
		String MarketContextD, MarketD;
		String FixtureID="90778997";
		Eventid="2:"+FixtureID+"?includeUndisplayed=true";
				
		//Reading Data from Conig File
		
		FixtureData = readMockValues(properties, "FixtureSnapshot");
		MarketD = readMockValues(properties, "MarketDelta");
		MarketContextD = readMockValues(properties, "MarketContextDelta");
		
		//Replace Date and ID to insert Fixture as a New Event
		
		FixtureData = FixtureData.replaceAll("2020-05-11", modifiedDate).replaceAll("1234567", FixtureID);
		MarketD=MarketD.replaceAll("1234567", FixtureID);
		MarketContextD=MarketContextD.replaceAll("1234567", FixtureID);
		
		//Converting Json to Proto
		
		Gson gson=new Gson();
		Fixture fixtureReconvertedFromJson =gson.fromJson(FixtureData, Fixture.class);
		MarketDelta marketDeltaConversion=gson.fromJson(MarketD, MarketDelta.class);
		MarketContextDelta marketContextDeltaConversion=gson.fromJson(MarketContextD, MarketContextDelta.class);
		
		//Sending Fixture Snapshot Message to Kafka
		
		KafkaProducerExample.sendMessage(0L, "Key_2020", fixtureReconvertedFromJson,"Fixture","2:"+FixtureID);
		
		Thread.sleep(20000);
		
		//Sending Market Context Delta Snapshot Message to Kafka for Resulting
		
		//KafkaProducerExample.sendMessage(0L, "Key_2020", marketDeltaConversion,"MarketDelta","2:"+FixtureID);
		
		//Thread.sleep(20000);
		
		//KafkaProducerExample.sendMessage(0L, "Key_2020", marketContextDeltaConversion,"MarketContextDelta");
			
		//Thread.sleep(20000);
		allResponse = pat + eventurl + Eventid + pat;
		
		//Getting Response from SiteServer API and validating the response
		
		io.restassured.response.Response ssResponse = RestAssured.get(eventurl + Eventid);
		int event = ssResponse.getStatusCode();
		System.out.println(pat + event + pat);
		globalResponseActive = ssResponse;
		System.out.println(globalResponseActive);
		Thread.sleep(8000);
		
		// Verify the Event Name
		
		try {
			ssResponse.then().body("SSResponse.event.@name.toString()", equalTo("FC Arsenal"+FixtureID+" - Daegu FC"+FixtureID));
		} catch (Exception e) {
			System.out.println("Event not inserted" + e.getMessage());
		}
	}
	
}

	
