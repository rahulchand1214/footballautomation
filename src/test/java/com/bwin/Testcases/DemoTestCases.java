package com.bwin.Testcases;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.bwin.Automation.Utilities.*;



public class DemoTestCases extends TestBase {

	public String Envtype;
	
	public  DemoTestCases()
	{
		Envtype=TestBase.getPathNames("DataConfig");
		
	}
	
	
	@BeforeClass 
	private void GetConfigData()
	{
		
          System.out.println("The Congif data for ENv is ::"+TestBase.getProperty(Envtype, "Environmenttype"));
	}
	
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
	LocalDateTime now = LocalDateTime.now();
	@BeforeTest
	public void Beforeexecution() {
		   
		   //System.out.println("Before ALl test Execution Time is :"+dtf.format(now));
		System.out.println("Before ALl test Execution Time is  :"+java.time.Clock.systemUTC().instant()); 
        		
		
	}
	
	
	@Test(priority = 3 ,dependsOnMethods = "TraditionalMarkets")
	public void HandiCapMarket()
	{
		
		System.out.println("This is Handicap Market test Execution details");
		
	}
	
	
	@Test(priority = 2,groups = {"HC"})
	public void OverUnderMarkets()
	{
		System.out.println("This is OverUnder  Market test Execution details");
		
	}
	
	@Test(priority = 1)
	public void TraditionalMarkets() throws InterruptedException
	{
		System.out.println("This is Triditional Market test Execution details");
		Thread.sleep(8000);
		//Assert.assertFalse(false,"Failed");
	}
	
	@Test(priority = 4)
	public void TraditionalMarkets2() throws InterruptedException
	{
		Assert.assertFalse(true);
		System.out.println("This is grouped test execution 1");
		Thread.sleep(8000);
	}
	
	@Test(priority = 5)
	public void TraditionalMarkets4() throws InterruptedException
	{
		System.out.println("This is grouped test execution 2");
		Thread.sleep(8000);
	}
	
	@AfterTest
	public void AfterExecutionMethod() 
	{
		//System.out.println("After All Test  the Execution Time is :"+dtf.format(now));
		Assert.assertFalse(false);
		System.out.println("After All Test  the Execution Time is :"+java.time.Clock.systemUTC().instant()); 
	}
	
	
}
