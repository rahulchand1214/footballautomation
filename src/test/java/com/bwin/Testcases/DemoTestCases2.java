package com.bwin.Testcases;
import java.sql.Time;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Timer;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.util.TimeUtils;

public class DemoTestCases2 {

	
	
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
	LocalDateTime now = LocalDateTime.now();
	@BeforeTest
	public void Beforeexecution() {
		   
		   //System.out.println("Before ALl test Execution Time is :"+dtf.format(now));
		System.out.println("Beforetests Execution Time is(Class 2)  :"+java.time.Clock.systemUTC().instant()); 
        		
		
	}
	
	
	@Test(priority = 2)
	public void SampleFirsttest()
	{
		System.out.println("This is the First test Execution details(Class 2)");
		
	}
	
	
	
	@Test(priority = 1)
	public void SampleSecondtest() throws InterruptedException
	{
		System.out.println("This is the Second test Execution details(Class 2)");
		Thread.sleep(8000);
	}
	
	@AfterTest
	public void AfterExecutionMethod() 
	{
		//System.out.println("After All Test  the Execution Time is :"+dtf.format(now));
		System.out.println("After All Test  the Execution Time is (Class 2):"+java.time.Clock.systemUTC().instant()); 
	}
	
	
}
